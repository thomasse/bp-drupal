class MainMenuClass{

  /**
   * A la construction du menu.
   * @param context
   * @param settings
   */
  attach(context, settings){
    this.menu = document.querySelector('.main-menu');
    this.initMenu(this.menu);
  }

  /**
   * Initialisation du menu.
   * @param menu
   */
  initMenu(menu) {
    // @todo.
  }
}


export default new MainMenuClass();
