<?php
require_once 'Base/ScriptsAbstract.php';


class DrupalUpdater extends ScriptsAbstract
{
  /**
   * {@inheritdoc}
   */
  public function __construct($argv)
  {
    parent::__construct($argv);
    // Liste des opérations
    $this
      ->composer($this->scriptOptions['type'] !== 'dev')
      ->configuration($this->scriptOptions['site']);
  }

  /**
   * Initialise composer.
   *
   * @param bool $noDevMode
   */
  public function composer($noDevMode = true)
  {
    // Installation de composer.
    $this->log('==== Composer ====');

    $this->exec(
      'composer install --no-interaction' . ($noDevMode ? ' --no-dev' : '')
    );

    return $this;
  }

  /**
   * Effectue les commandes drush de maj de conf.
   *
   * @return $this
   */
  public function configuration($site = null)
  {
    if ($site) {
      foreach (glob('./web/sites/' . $site . '/settings.php') as $sitePath) {
        $this->configurationSite($sitePath);
      }
    } else {
      foreach (glob('./web/sites/*/settings.php') as $sitePath) {
        $this->configurationSite($sitePath);
      }
    }


    return $this;
  }

  protected function configurationSite($site)
  {
    $pathData = pathinfo($site);

    $this->log("============================================ ");
    $this->log("===== " . basename($pathData['dirname']) . " ====== ");


    // Mode maintenance : on
    $this->log("== Maintenance : on");
    $this->drush('state:set system.maintenance_mode 1 --input-format=integer', $pathData['dirname']);


    $this->log("== CR");
    $this->drush('cr', $pathData['dirname']);

    $this->log("== CIM");
    $this->drush('cim -y', $pathData['dirname']);

    // Update de db
    $this->log("== UPDB");
    $this->drush('updb -y', $pathData['dirname']);

    // Mode maintenance : on
    $this->log("== Maintenance : off");
    $this->drush('state:set system.maintenance_mode 0 --input-format=integer', $pathData['dirname']);

    $this->log("== CR");
    $this->drush('cr', $pathData['dirname']);
  }
}

(new DrupalUpdater($argv));
