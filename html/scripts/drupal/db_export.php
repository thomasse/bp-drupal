<?php
require_once 'Base/DBExchangeAbstract.php';


class DBExport extends DBExchangeAbstract
{
  /**
   * {@inheritdoc}
   */
  public function __construct($argv)
  {
    parent::__construct($argv);

    $this->save($this->scriptOptions['site']);
  }

  /**
   * @param null $site
   */
  protected function save($site = null)
  {
    $dir = $this->getExchangeRep() . ($this->scriptOptions['dir'] ?: date('Ymd-His'));
    if ($site) {
      foreach (glob('./web/sites/' . $site . '/settings.php') as $sitePath) {
        $this->saveOneSite($sitePath, $dir);
      }
    } else {
      foreach (glob('./web/sites/*/settings.php') as $sitePath) {
        $this->saveOneSite($sitePath, $dir);
      }
    }
  }

  /**
   * Sauvegarde la db d'un site.
   * @param $sitePath
   */
  private function saveOneSite($sitePath, $dirname)
  {
    $this->exec('mkdir %s -p', $dirname);

    // Récupéraion du nom du fichier sql à importer.
    $fileName = $this->getExchangeFileFromSitePath($sitePath, $dirname);

    $this->drush('cr', dirname($sitePath));
    $this->drush('sql-dump > ' . $fileName, dirname($sitePath));
  }


}

(new DBExport($argv));
