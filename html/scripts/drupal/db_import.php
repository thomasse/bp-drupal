<?php
require_once 'Base/DBExchangeAbstract.php';


class DBExport extends DBExchangeAbstract
{
  /**
   * {@inheritdoc}
   */
  public function __construct($argv)
  {
    parent::__construct($argv);

    $site = isset($this->scriptOptions['site']) && !empty($this->scriptOptions['site']) ? $this->scriptOptions['site'] : '*';
    $this->import($this->scriptOptions['dir'], $site);
  }

  /**
   * @param null $site
   */
  protected function import($dirname = null, $site = '*')
  {
    $dir = $this->getExchangeRep() . $dirname;
    if (!$dirname || !file_exists($dir) || !is_dir($dir)) {
      throw new \Exception('Pas de rep d\'import défini');
    }
    foreach (glob('./web/sites/' . $site . '/settings.php') as $sitePath) {
      $this->importOneSite($sitePath, $dir);
    }
  }

  /**
   * Sauvegarde la db d'un site.
   * @param $sitePath
   */
  private function importOneSite($sitePath, $dirname)
  {
    // Récupéraion du nom du fichier sql à importer.
    $fileName = $this->getExchangeFileFromSitePath($sitePath, $dirname);

    // Erreur si pas de fichier sql.
    if (!file_exists($fileName)) {
      throw new \Exception('Aucun fichier à importer : ' . $dirname);
    }

    $this->drush('sqlc < ' . $fileName, dirname($sitePath));
  }

}

(new DBExport($argv));
