<?php

/**
 * Class ScriptsBase
 *
 * Définition des fonctions de bases pour les scripts d'automation.
 */
abstract class ScriptsAbstract
{
  /**
   * Options du script.
   * @var array
   */
  protected $scriptOptions = [];

  /**
   * {@inheritdoc}
   */
  public function __construct($argv)
  {
    $this->initScriptOptions($argv);
  }

  /**
   * Initialise les options de script.
   * @param array $argv
   */
  protected function initScriptOptions(array $argv)
  {
    foreach ($argv as $arg) {
      if (strpos($arg, '=') > 0) {
        list($key, $value) = explode('=', $arg);
        $this->scriptOptions[$key] = $value;
      }
    }
  }

  /**
   * Execute une commande drushd ans dirname.
   *
   * @param string $action
   *   La commande drush à executer.
   * @param string $dirname
   *   Le répertoire d'execution.
   */
  protected function drush($action, $dirname = './web/sites/default/')
  {
    return $this->exec(
      'cd %s && ../../../vendor/bin/drush %s',
      $dirname,
      $action
    );
  }

  /**
   * Log
   *
   * @param string $string
   *   Log
   *
   * @return $this
   */
  protected function log($string)
  {
    echo $string . "\n";
    return $this;
  }

  /**
   * Execute une commande.
   *
   * @param $cmd
   * @param mixed ...$args
   *
   * @return $this
   */
  protected function exec($cmd, ...$args)
  {
    $cmd = sprintf($cmd, ...$args);
    $this->log($cmd);
    $output = [];
    exec($cmd, $output);

    return $output;
  }
}
