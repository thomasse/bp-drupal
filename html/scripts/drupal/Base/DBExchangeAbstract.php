<?php

require_once "ScriptsAbstract.php";

abstract class DBExchangeAbstract extends ScriptsAbstract
{
  /**
   * Répertoire du .env global.
   */
  const ENV_DIR = '/var/www/data/';

  const DB_REP = 'db/';

  /**
   * Retourne le répertoire d'échange de db.
   * @return string
   */
  public function getExchangeRep()
  {
    $path= static::ENV_DIR.static::DB_REP;
    if( !file_exists($path) ){
      mkdir($path);
    }
    return $path;
  }

  /**
   * Retourne le nom de la db en fonctin du sitePath.
   *
   * @param $sitePath
   *
   * @return string
   */
  public function getDatabasenameFromSitePath($sitePath)
  {
    // récuépration du nom de la db en fonction du drush status.
    $value = $this->drush('status', dirname($sitePath));
    $database = array_filter(
      $value,
      function ($item) {
        return strpos($item, 'DB name') !== false;
      }
    );
    $database = trim(explode(':', reset($database))[1]);
    return $database;
  }

  /**
   * Retourne le path du fichier d'échange en fonction du sitepath.
   * @param $sitePath
   * @param $dirname
   */
  public function getExchangeFileFromSitePath($sitePath, $dirname)
  {
    $database = $this->getDatabasenameFromSitePath($sitePath);
    return $dirname . '/' . str_replace('_', '-', $database) . '.sql';
  }
}
