const glob = require('glob');
const path = require('path');
const {doExec, getToken} = require('./src/initToken');

/**
 * BUild les assets de tous les répertoire de themes contenu un ./gulp après avoir deamndé un token au nexus.
 */
(() => {
    getToken(() => {
        console.log("===> Constrution des assets");

        const reps = glob.sync('../../web/themes/custom/*/libraries/assets/gulp');

        console.log('Liste des répertoire à parser : ');
        console.log(reps);

        // On parcourt les éléments à builder.
        reps.forEach((item) => {

            const cmd = `cd ${path.dirname(item)} && npm i --only=prod && ./gulp build --production && rm -rf node_modules`;
            console.log(cmd)
            doExec(cmd)
                .then(ok => {
                    console.log('==== Build OK pour ', glob.sync(path.dirname(path.join(item, '../'))));
                })
                .catch(err => {
                    console.error('Erreur lors du build')
                    console.error(err);
                })
        })
    });
})();


