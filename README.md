Drupal Stack
============

Everything is available thanks to `make` commands.

* `make up`: Launch containers
* `make stop`: Stop containers
* `make prune`: Remove containers
* `make ps`: See current containers launched
* `make shell`: Access to bash on PHP container
* `make drush`: Launch drush command
* `make logs`: Show logs
* `make composer`: Launch composer command
* `make site-install`: Install site
* `make site-update`: Update site
* `make build`: Rebuild containers
* `make xdebug-enable`: Enable xdebug
* `make xdebug-disable`: Disable xdebug
* `make xdebug-install`: Install xdebug
* `make xdebug-ini`: Install xdebug.ini config in .docker/php
* ̀`disable-opcache`: Disable opcache in case of php refresh latencies
* ̀`enable-opcache`: Enable opcache
# For development

## Première installation.

### Utiliser drupal-builder si possible (gitlab.consertotech.pro:drupal/drupal-builder.git)
drupal-builder est un outil sur mesure qui vous permet de lancer l'installation d'un nouveau projet rapidement.
 1. Placez vous dans le répertoire qui contiendra votre projet et lancez la commande : 
 `drupal-builder create {project_name}` (pour ne pas intéragir ajoutez l'option ` --y`)
 2. Suivez les instructions, à la fin du process, il vous fournira l'url de connexion au BO en tant qu'admin.
 
### Manuellement
#### Initialisation du projet
1. Clonez le repo 'git clone git@gitlab.consertotech.pro:tech/bp-drupal.git {project_name}'
2. Placez vous dans le rep de travail `cd {project_name}`
3. Remplacez `.gitignore.example` en `.gitignore` 

#### Initialisation Docker
4. Créez `.env` sur le modèle de  `.env.example`
5. Renseignez __toutes__ les occurences des variables {{project_name}}, {{web_gid}}, {{web_uid}} présentes dans `.env`  
Pour connaitre {{web_gid}}, {{web_uid}}, lancez la commande `id` dans votre terminal. Sous linux, c'est souvent 1000 et 1000.
6. Lancez le build des containers avec `make build`
7. Installez les packages composer `cd www/ && composer insall`
8. Lancez les containers `make up`

#### Initialisation du profile d'install.
9. Pour utiliser le profil d'install, déplacez tous les répertoires présents dans `www/web/profiles/custom/__templates` dans
`www/web/profiles/custom`.
10. Renseignez toutes les occurences des variables `{{project_name}}` dans tous les fichiers et noms de répertoires. 

#### Initialisation du module du projet.
11. Pour créer un module de projet, déplacez tous les répertoires présents dans `www/web/modules/custom/__templates` dans
`www/web/modules/custom`.
12. Renseignez toutes les occurences des variables `{{project_name}}` dans tous les fichiers et noms de répertoires.

#### Initialisation des thèmes admin et front.
13. Pour créer les thèmes du projet, déplacez tous les répertoires présents dans `www/web/themes/custom/__templates` dans
`www/web/themes/custom`.
14. Renseignez toutes les occurences des variables `{{project_name}}` dans tous les fichiers et noms de répertoires.

#### Installation du projet.
15. Lancez la commande d'install du projet
 - Si vous utilisez un profil d'install custom :  
    `make site-install {project_name}_profile`
 - sinon pour un profil d'install standard (déconseillé)
    `make site-install`
 16. Accédez au site via `make drush uli` 

## DX
### PHPStorm
#### Ajouter des watchers sur les fichiers yml et lancer un drush cr.
Pour automatiser le drush cr en fonction d'une modif de fichiers yml de module.

1. Aller dans les paramètres > Tools > File Watcher
2. Ajouter un watcher "<custom>"
3. Créer un scop dédié aux modules : 
    1. Créer un scope "Local"
    2. Nommer "Modules"
    3. Renseigner la pattern "file[{{project_name}}]:www/web/modules/custom/*/*.yml"
    
4. Renseigner  
Nom: Services (ou ce que vous voulez)
Type: Yml
Scope: Modules
Program: /bin/make
Arguments: drush cr
Working directory: $ProjectFileDir$
Show console: on error
