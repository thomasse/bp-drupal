include .docker/drupal/docker-drupal.mk
include .docker/traefik/traefik.mk
include .docker/mariadb/mariadb.mk
include .docker/node/node.mk


all-up: ## All up
	- make traefik-up
	- make mariadb-up
	- make up
	- make node-up


all-stop: ## All up
	- make stop
	- make mariadb-halt
	- make traefik-halt
	- make node-stop
