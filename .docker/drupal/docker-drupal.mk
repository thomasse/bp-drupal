include .env

.PHONY: up down stop prune ps shell drush logs composer sh install update build

default: up

DRUPAL_ROOT ?= /var/www/html/web
CONNECT_SERVER ?= docker exec -ti --user www-data $(COMPOSE_PROJECT_NAME)_server
CONNECT_SERVER_ROOT ?= docker exec -ti --user root $(COMPOSE_PROJECT_NAME)_server

help:  ## Affiche l'aide du Makefile
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'



##
#############################    Docker    #####################################

dev-install: ## Installation du projet
	##### Build des containers
	make build-all

	#### Import de la db si existante.
	- make db-import dir=init

	- make drush uli

build-all: ## Build tous les containers nécessaires.
	#### Build de maria DB si il n'est pas déjà up.
	- make mariadb-up

	### Build de traefik si il n'est pas déjà up.
	- make traefik-up

	#### Build apache
	make up

	#### Création de la db
	- make db-create

	#### Load des sources
	- make composer install

build: ## Build les containers
	@echo "Rebuild containers $(COMPOSE_PROJECT_NAME)..."
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} build

up: ## Lance les containers
	@echo "Starting up containers for $(COMPOSE_PROJECT_NAME)..."
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} up -d

pull: ## Pull donw les containers ?
	@echo "Pull down containers for $(COMPOSE_PROJECT_NAME)..."
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} pull --parallel

stop: ## Arrête les containers
	@echo "Stopping containers for $(COMPOSE_PROJECT_NAME)..."
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} stop

halt: stop

prune: ## Supprime les containers
	@echo "Removing containers for $(COMPOSE_PROJECT_NAME)..."
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} down -v --remove-orphans

rmi: ## Supprime les containers et images
	@echo "Removing containers for $(COMPOSE_PROJECT_NAME)..."
	@if [ "$(shell bash -c 'read -s -p "Vous allez supprimer les images, êtes-vous sûr? [y/n]: " pwd; echo $$pwd')" = "y" ]; then\
	    @docker-compose -f ${DRUPAL_COMPOSE_FILE} stop;\
	    make prune;\
		docker-compose -f ${DRUPAL_COMPOSE_FILE} down --rmi local -v;\
	fi

ps: ## Affiche les infos des containers liés au projet.
	@docker ps --filter name='$(COMPOSE_PROJECT_NAME)*'

build-no-cache: ## build sans cache.
	@echo "Rebuild containers $(COMPOSE_PROJECT_NAME)..."
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} build --no-cache


##
#############################    Containers    #####################################
connect-apache: ## Connection auc ontainer apache.
	$(CONNECT_SERVER) bash $(filter-out $@,$(MAKECMDGOALS))
#	$(CONNECT_SERVER_ROOT) bash $(filter-out $@,$(MAKECMDGOALS))

##
#############################    Drupal    #####################################

drush: ## Commande drush.
	$(CONNECT_SERVER) vendor/bin/drush -r /var/www/html/web --uri=${SITE_DOMAIN} $(filter-out $@,$(MAKECMDGOALS))

composer: ## Commande composer.
	$(CONNECT_SERVER) php -d memory_limit=-1 "/usr/local/bin/composer" $(filter-out $@,$(MAKECMDGOALS))

site-install: ## Installation du site . Précisez le profile à utiliser ex: make site-install mon_profile
	$(CONNECT_SERVER) ./vendor/bin/drush si --db-url=mysql://${MYSQL_USER_DRUPAL}:${MYSQL_PASSWORD_DRUPAL}@${MYSQL_HOST_DRUPAL}:${MYSQL_PORT_DRUPAL}/${MYSQL_DATABASE_DRUPAL} -y -r /var/www/html/web $(filter-out $@,$(MAKECMDGOALS))

site-update: ## Fait un updb de tous les sites.
	$(CONNECT_SERVER) php 'scripts/drupal/update.php' type=dev site=$(filter-out $@,$(MAKECMDGOALS))

db-import: ## Export de db. (ex: `make db-import dir=destination` ou précisez un seul site `make db-import dir=destination site=default`  )
	$(CONNECT_SERVER) php 'scripts/drupal/db_import.php' site=$(site) dir=$(dir)

db-export: ## Export de db. (ex: `make db-export dir=destination` ou précisez un seul site `make db-export dir=destination site=default`  )
	$(CONNECT_SERVER) php 'scripts/drupal/db_export.php' site=$(site) dir=$(dir)

db-create: ## Création de la base de données.
	$(CONNECT_SERVER_ROOT) mysql -h ${MYSQL_HOST_DRUPAL} -u root -proot -e "CREATE DATABASE IF NOT EXISTS $(MYSQL_DATABASE_DRUPAL); GRANT ALL PRIVILEGES ON $(MYSQL_DATABASE_DRUPAL).* TO '$(MYSQL_USER_DRUPAL)'@'%' WITH GRANT OPTION;"

##
#############################    Conf PHP    #####################################

disable-opcache: ## Désactiver opcache
	$(CONNECT_SERVER_ROOT) cp /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini /usr/local/etc/php/conf.d/save_opcache
	$(CONNECT_SERVER_ROOT) mv /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini /usr/local/etc/php/conf.d/dis_opcache
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} stop php
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} start php

enable-opcache: ## Résactiver opcache
	$(CONNECT_SERVER_ROOT) mv /usr/local/etc/php/conf.d/dis_opcache /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} stop php
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} start php

xdebug-enable: ## Activer xdebug
	$(CONNECT_SERVER_ROOT) mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.disabled /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} stop php
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} start php

xdebug-disable: ## Désactiver xdebug
	$(CONNECT_SERVER_ROOT) mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.disabled
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} stop php
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} start php

xdebug-install: ## Installer xdebug
	@echo "Install xdebug"
	$(CONNECT_SERVER_ROOT) pecl install xdebug
	@docker cp ./.docker/php/xdebug.ini drupal:/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} stop php
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} start php

xdebug-ini: ## Init xdebug.
	@echo "Replace xdebug.ini"
	@docker cp ./.docker/php/xdebug.ini $(COMPOSE_PROJECT_NAME)_server:/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} stop php
	@docker-compose -f ${DRUPAL_COMPOSE_FILE} start php

# https://stackoverflow.com/a/6273809/1826109
%:
	@:
