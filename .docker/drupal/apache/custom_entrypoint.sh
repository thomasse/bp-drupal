#!/bin/sh

if [ "prod" = "${ENV}" ]
then
  # wait bdd is up
  if [ "true" = "${IGNORE_CHECK_BDD}" ]
  then
      echo ""
      echo "No check if BDD is UP"
      echo ""
  else
      WAIT_TIMEOUT=300
      echo ""
      echo "Check if database is up (HOST / PORT / USER / DB_NAME = ${MYSQL_HOST_DRUPAL} / ${MYSQL_PORT_DRUPAL_EXTERNAL} / ${MYSQL_USER_DRUPAL} / ${MYSQL_DATABASE_DRUPAL}) (Timeout = ${WAIT_TIMEOUT}s)"
      echo ""

      NEXT_WAIT_TIME=0
      until mysql -h "${MYSQL_HOST_DRUPAL}" -P"${MYSQL_PORT_DRUPAL_EXTERNAL}" -u"${MYSQL_USER_DRUPAL}" -p"${MYSQL_PASSWORD_DRUPAL}" ${MYSQL_DATABASE_DRUPAL}
      do
        echo "Database is unavailable - sleeping ($NEXT_WAIT_TIME)"
        NEXT_WAIT_TIME=$(( $NEXT_WAIT_TIME + 1 ))
        sleep 1
        if [ ${NEXT_WAIT_TIME} -gt ${WAIT_TIMEOUT} ]
        then
          echo "Waiting timeout exceeded"
          kill -s 9 1
          exit 111
        fi
      done

      echo ""
      echo "Database is UP"
      echo ""

  fi

  # migrate
  #echo "## RUN UPDATES"
  su -s /bin/bash -c "php /var/www/${APP_DIR}/scripts/drupal/update.php" www-data &
fi

# run entrypoint
echo ""
echo "## RUN DEFAULT ENTRYPOINT"
docker-php-entrypoint apache2-foreground "$@"


